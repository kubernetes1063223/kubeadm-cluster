Here infrastructure template for kubeadm task.
Uses Vagrant and ubuntu.

# Deploy multi-masters HA cluster with `kubeadm`, `vagrant` and `ubuntu`.

Скрипты из этого репозитория облегчают процесс установления кластера производственного уровня.

- балансировщика
- 3 мастер-нод
- 2 рабочих узлов

### Склонируй этот репо
По умолчанию он настроен на создание 2 мастеров, 1 воркера и 1 лоадбаленсера.

```
cd kubeadm-cluster
vagrant up
```

Поднимутся VMs. Их адреса:
- 192.168.66.1X - для мастеров
- 192.168.66.2X - для рабочих
- 192.168.66.30 - для балансировщика

Т.е. `controlplane01` будет 192.168.66.11, `controlplane02` будет 192.168.66.12 и т.д.

Но сачала давай познакомимся с окружением

### Окружение
Ты можешь подключиться прямо к виртуальной машине с помощью
```
vagrant ssh node01
```
Или сделай команду:
```
vagrant ssh-config
```
Это покажет тебе адреса VMs, их порты ssh и пути к ключам, чтобы подключиться к ним из твоего любимого ssh-клиента.

### Балансировщик
Для сетапа высокой доступности необходимый элемент, без него не будет полноценной `HA`.
В этом сетапе мы используем `HAproxy`, давай ее настроим.

##### Выполняется в VM `lb`
#
```
sudo apt update
sudo apt install -y haproxy
```
Далее внесем правки в `/etc/haproxy/haproxy.cfg`, добавив в конец следующее:
```
frontend kubernetes-frontend
    bind 192.168.66.30:6443
    mode tcp
    option tcplog
    default_backend kubernetes-backend

backend kubernetes-backend
    mode tcp
    option tcp-check
    balance roundrobin
    server controlplane01 192.168.66.11:6443 check fall 3 rise 2
    server controlplane02 192.168.66.12:6443 check fall 3 rise 2
    server controlplane03 192.168.66.13:6443 check fall 3 rise 2
```
Теперь балансировщик будет пересылать трафик на один из трех мастеров. И он достаточно умный не послылать трафик, если мастер в данный момент недоступен. 
```
sudo systemctl restart haproxy
```
Мы закончили с балансировщиком, он нам больше не понадобится.

НАСТРОЙКА КЛАСТЕРА 

1. Провреить синхранизацию времени (sudo timedatectl timezone)
-----------------------------------------------------------------------------
2. Зайти на офф сайт кубера (устновка кластреа через Kubeadm)
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
-----------------------------------------------------------------------------
3. Выключить SWAP - sudo swapoff (на каждом узле)
-----------------------------------------------------------------------------
4. Заходим в container runtime и вкючаем IP_FORWARDING

# sysctl params required by setup, params persist across reboots
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.ipv4.ip_forward = 1
EOF

# Apply sysctl params without reboot
sudo sysctl --system

-----------------------------------------------------------------------------
5. Выбираем наш container runtime (example containerd)
-----------------------------------------------------------------------------
6. Устанвливаем containerd на каждом узле согласно оф. документации
https://github.com/containerd/containerd/blob/main/docs/getting-started.md
-----------------------------------------------------------------------------
7. После уствновки проверям status и включаем автостарт
sudo systemctl status containerd
sudo systemctl enable containerd
-----------------------------------------------------------------------------
8. Настраиваем драйвер Cgroups для containerd на каждом узле
заходим в файл /etc/containerd/config.toml и пишим примерно следующее

version = 2

[plugins]
  [plugins."io.containerd.grpc.v1.cri"]
    [plugins."io.containerd.grpc.v1.cri".containerd]
      [plugins."io.containerd.grpc.v1.cri".containerd.runtimes]
        [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
          runtime_type = "io.containerd.runc.v2"
          [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
            SystemdCgroup = true

-----------------------------------------------------------------------------
9. Делаем рестарт containerd
sudo systemctl restart containerd
-----------------------------------------------------------------------------
10. Installing kubeadm, kubelet and kubectl
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
-----------------------------------------------------------------------------
11. To initialize the control-plane node run (Запускаем только на master-node)
sudo kubeadm init --pod-network-cidr=10.244.0.0/24 --apiserver-advertise-addres=192.168.56.11
-----------------------------------------------------------------------------
12. Неочищаем вывод после инициализации нужно будет выполнить следующее
To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
-----------------------------------------------------------------------------
13. Устанавливаем сетевой плагин
You should now deploy a Pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  /docs/concepts/cluster-administration/addons/
Будем устанвливать weawe net (устновка из оф доки)
https://rajch.github.io/weave/
-----------------------------------------------------------------------------
14. Добовляем ноды в кластер (на worker-node)
kubeadm join 192.168.56.11:6443 --token dsgmeifmse8358gefvds --discovery-token-ca-cert-hash sha256:safjsdbfwefb748tyf4cm78fyt4x7tymc3478rym7xm23rc32c823nrt82
-----------------------------------------------------------------------------
15. Настройка доступа к кластеру

На мастер-узле:
Если вы хотите подключиться к кластеру с удаленной машины (рабочей станции), вам нужно скопировать файл конфигурации:
scp username@master-node:~/.kube/config ~/.kube/config
Замените username на имя пользователя на мастер-узле, а master-node на IP-адрес или имя хоста мастер-узла.

Настройка kubeconfig
Если вы использовали нестандартное место для хранения файла конфигурации и не скопировали его в ~/.kube/config, вы можете указать путь к файлу, задав переменную окружения:
export KUBECONFIG=/path/to/admin.conf

Подключение к кластеру
Теперь вы можете использовать kubectl для подключения к вашему кластеру. Проверьте доступность кластера с помощью следующей команды:
kubectl get nodes
Если вы видите список узлов вашего кластера, значит всё настроено правильно.
